use clap_verbosity_flag::Verbosity;
use tw_errors::{SourceError, TWError};
use tw_plugin::Plugin;
use twa_cpp::PreprocessorPlugin;

use crate::settings::source::{LanguageCommands, SourceCommand};

/// Execute source command
pub struct SourceCmdExecutor {}

impl SourceCmdExecutor {
    pub fn execute(
        source_cmd: &SourceCommand,
        verbosity: &Verbosity,
    ) -> Result<(), TWError> {
        let mut plugin: Box<dyn Plugin> = match &source_cmd.language {
            LanguageCommands::Cpp(settings) => {
                Box::new(PreprocessorPlugin::new(&settings.cpp, verbosity)?)
            }
            _ => {
                return Err(TWError::SourceError(SourceError::NotImplemented(format!(
                    "Source command '{}'",
                    source_cmd.language.to_string()
                ))))
            }
        };
        plugin.execute()?;

        Ok(())
    }
}
