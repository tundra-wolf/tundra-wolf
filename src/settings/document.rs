use std::fmt::{Display, Formatter, Result as FmtResult};

use clap::{Args, Subcommand};
use tw_errors::TWError;

// Commands supported by the documentation command line interface
#[derive(Debug, Subcommand)]
pub enum DocumentCliCommands {
    Add(AddCommand),
    Generate(GenerateCommand),
    Read(ReadCommand),
    Update(UpdateCommand),
}

impl Display for DocumentCliCommands {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> FmtResult {
        write!(
            fmt,
            "Command '{}'",
            match self {
                Self::Add(_) => "Add Document",
                Self::Generate(_) => "Generate Documents",
                Self::Read(_) => "Read Document",
                Self::Update(_) => "Update Documents",
            }
        )
    }
}

/// Add a document
#[derive(Args, Debug)]
pub struct AddCommand {}

impl AddCommand {
    pub fn validate(mut self) -> Result<AddCommand, TWError> {
        Ok(self)
    }
}

/// Generate documents
#[derive(Args, Debug)]
pub struct GenerateCommand {}

/// Read a document in HTML
#[derive(Args, Debug)]
pub struct ReadCommand {}

/// Update documents
#[derive(Args, Debug)]
pub struct UpdateCommand {}
