pub mod cli;
pub mod document;
pub mod source;

use std::path::PathBuf;

use clap::{Parser, ValueEnum};
use clap_verbosity_flag::Verbosity;
use tw_errors::TWError;

/// Common arguments
#[derive(Debug, Parser)]
pub struct CommonArguments {
    /// Project ID
    project_id: String,

    /// Action on project
    #[arg(
        long = "action",
        short = 'A',
        default_value_t = ProjectAction::Continue,
        value_enum
    )]
    action: ProjectAction,

    /// Configuration file
    #[arg(long = "configuration-file", short = 'C')]
    configuration_file: Option<PathBuf>,

    #[command(flatten)]
    verbose: Verbosity,
}

/// Project action.
#[derive(Clone, Debug, ValueEnum)]
pub enum ProjectAction {
    Continue,
    Reset,
    Restart,
}

/// The Tundra Wolf UI provides all the project's functionality in a menu driven user
/// interface.
#[derive(Debug, Parser)]
#[command(about, author, long_about = None, version)]
pub struct UISettings {
    #[command(flatten)]
    common_arguments: CommonArguments,
}

impl UISettings {
    /// Get the settings. This function performs parsing and validation on the arguments
    /// provided by the user.
    pub fn get() -> Result<UISettings, TWError> {
        let settings = Self::parse();

        settings.validate()
    }

    /// Validate the consistency of the runtime configuration.
    fn validate(self) -> Result<UISettings, TWError> {
        Ok(self)
    }
}
