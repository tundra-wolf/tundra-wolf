use std::error::Error;
use std::process::exit;

use clap::Parser;
use clap_verbosity_flag::Verbosity;
use tundra_wolf::interactive::InteractiveCmdExecutor;
use tundra_wolf::settings::cli::CliCommands;
use tundra_wolf::source::SourceCmdExecutor;
use tw_errors::{InternalError, TWError};

/// The Tundra Wolf command line interface provides all functionality of the project in
/// the terminal.
#[derive(Debug, Parser)]
#[command(about, author, version, long_about = None)]
struct Settings {
    #[command(subcommand)]
    command: CliCommands,

    #[command(flatten)]
    verbose: Verbosity,
}

impl Settings {
    /// Get the settings. This function performs parsing and validation on the arguments
    /// provided by the user.
    pub fn get() -> Result<Settings, TWError> {
        let settings = Self::parse();

        settings.validate()
    }

    /// Validate the consistency of the runtime configuration.
    fn validate(mut self) -> Result<Settings, TWError> {
        self.command = match self.command {
            CliCommands::Source(command) => CliCommands::Source(command.validate()?),
            _ => self.command,
        };

        Ok(self)
    }
}

/// Main fucntion for the Tundra Wolf command line interface
fn main() {
    // Get settings
    let settings = match Settings::get() {
        Ok(settings) => settings,
        Err(error) => {
            let source = if let Some(source) = error.source() {
                source
            }
            else {
                &InternalError::NoSourceInError
            };
            eprintln!("{}", source.to_string());
            exit(1)
        }
    };

    // Process commands
    let error = match &settings.command {
        CliCommands::Interactive(command) | CliCommands::Run(command) => {
            // Interactive/Run command
            match InteractiveCmdExecutor::execute(&command, &settings.verbose) {
                Ok(status) => exit(status),
                Err(error) => error,
            }
        }
        CliCommands::Source(command) => {
            // Source command
            match SourceCmdExecutor::execute(&command, &settings.verbose) {
                Ok(()) => exit(0),
                Err(error) => error,
            }
        }
        _ => {
            // Unimplemented command
            eprintln!(
                "{}",
                InternalError::NotImplemented(settings.command.to_string()).to_string()
            );
            exit(1)
        }
    };

    // Process source of the error
    let source = if let Some(source) = error.source() {
        source
    }
    else {
        &InternalError::NoSourceInError
    };
    eprintln!("{}", source.to_string());

    exit(1)
}
