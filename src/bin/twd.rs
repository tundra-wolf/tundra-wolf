use std::error::Error;
use std::process::exit;

use clap::Parser;
use clap_verbosity_flag::Verbosity;
use tundra_wolf::settings::document::DocumentCliCommands;
use tw_errors::{InternalError, TWError};

/// The Tundra Wolf command line interface provides all functionality of the project in
/// the terminal.
#[derive(Debug, Parser)]
#[command(about, author, version, long_about = None)]
struct Settings {
    #[command(subcommand)]
    command: DocumentCliCommands,

    #[command(flatten)]
    verbose: Verbosity,
}

impl Settings {
    /// Get the settings. This function performs parsing and validation on the arguments
    /// provided by the user.
    pub fn get() -> Result<Settings, TWError> {
        let settings = Self::parse();

        settings.validate()
    }

    /// Validate the consistency of the runtime configuration.
    fn validate(mut self) -> Result<Settings, TWError> {
        self.command = match self.command {
            DocumentCliCommands::Add(command) => {
                DocumentCliCommands::Add(command.validate()?)
            }
            _ => self.command,
        };

        Ok(self)
    }
}

/// Main fucntion for the Tundra Wolf command line interface
fn main() {
    // Get settings
    let settings = match Settings::get() {
        Ok(settings) => settings,
        Err(error) => {
            let source = if let Some(source) = error.source() {
                source
            }
            else {
                &InternalError::NoSourceInError
            };
            eprintln!("{}", source.to_string());
            exit(1)
        }
    };
}
