use std::fmt::{Display, Formatter, Result as FmtResult};
use std::path::PathBuf;

use clap::{Args, Subcommand};
use tw_errors::TWError;
use twa_c::settings::CSettings;
use twa_cpp::settings::CppSettings;
use twa_vb::settings::VisualBasicSettings;

/// Process a single source file
#[derive(Args, Debug)]
pub struct SourceCommand {
    #[command(subcommand)]
    pub language: LanguageCommands,

    /// Configuration file
    #[arg(long = "configuration-file", short = 'C')]
    configuration_file: Option<PathBuf>,
}

impl SourceCommand {
    pub fn validate(mut self) -> Result<SourceCommand, TWError> {
        // Validate language settings
        self.language = match self.language {
            LanguageCommands::Cpp(command) => LanguageCommands::Cpp(command.validate()?),
            _ => self.language,
        };

        Ok(self)
    }
}

/// Language parameters
#[derive(Debug, Subcommand)]
pub enum LanguageCommands {
    C(CSubcommand),
    Cpp(CppSubcommand),
    Vb(VisualBasicSubcommand),
}

impl Display for LanguageCommands {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> FmtResult {
        write!(
            fmt,
            "Language '{}'",
            match self {
                LanguageCommands::C(_) => "C",
                LanguageCommands::Cpp(_) => "C/C++ Preprocessor",
                LanguageCommands::Vb(_) => "Visual Basic",
            }
        )
    }
}

/// C language subcommand
#[derive(Args, Debug)]
pub struct CSubcommand {
    #[command(flatten)]
    c: CSettings,
}

/// C/C++ preprocessor subcommand
#[derive(Args, Debug)]
pub struct CppSubcommand {
    #[command(flatten)]
    pub cpp: CppSettings,
}

impl CppSubcommand {
    pub fn validate(mut self) -> Result<CppSubcommand, TWError> {
        // Validate preprocessor setitngs
        self.cpp = self.cpp.validate()?;

        Ok(self)
    }
}

/// Visual Basic language subcommand
#[derive(Args, Debug)]
pub struct VisualBasicSubcommand {
    #[command(flatten)]
    vb: VisualBasicSettings,
}
