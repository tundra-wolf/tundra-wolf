use std::process::exit;

use tundra_wolf::settings::UISettings;

// Main function for the Tundra Wolf GUI
fn main() {
    let settings = match UISettings::get() {
        Ok(settings) => settings,
        Err(error) => {
            eprintln!("{}", error.to_string());
            exit(1)
        }
    };
}
