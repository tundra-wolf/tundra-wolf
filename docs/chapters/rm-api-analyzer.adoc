////
	[Chapter] Analyzer Plugin API (`libs/tw-analyzer`)
////

= Analyzer Plugin API (`libs/tw-analyzer`)

This library contains the common components for the analyzer plugins. It uses the libraries _Data_, _Errors_ and _Plugin_ which contains components used by all plugin or the entire code base.

////
[[
	./build.sh doc generate-api libs/tw-analyzer --type asciidoc --highest-level section
]]
////
