use clap_verbosity_flag::Verbosity;
use tw_errors::TWError;
use tw_shell::Shell;

use crate::settings::cli::InteractiveCommand;

/// Interactive command executor
pub struct InteractiveCmdExecutor {}

impl InteractiveCmdExecutor {
    pub fn execute(
        interactive_cmd: &InteractiveCommand,
        verbosity: &Verbosity,
    ) -> Result<i32, TWError> {
        let mut shell = Shell::new(interactive_cmd.into())?;

        shell.execute()
    }
}
