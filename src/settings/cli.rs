use std::fmt::{Display, Formatter, Result as FmtResult};
use std::path::PathBuf;

use clap::{Args, Subcommand};
use tw_shell::ShellSettings;
use twa_projects::settings::ProjectKindArgument;

use crate::settings::source::SourceCommand;
use crate::settings::CommonArguments;

/// Commands supported by the command line interface
#[derive(Debug, Subcommand)]
pub enum CliCommands {
    Analyse(AnalyseCommand),
    Configure(ConfigCommand),
    Convert(ConvertCommand),
    Create(CreateCommand),
    Interactive(InteractiveCommand),
    Package(PackageCommand),
    Refactor(RefactorCommand),
    Run(InteractiveCommand),
    Source(SourceCommand),
    Test(TestCommand),
}

impl Display for CliCommands {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> FmtResult {
        write!(
            fmt,
            "Command '{}'",
            match self {
                Self::Analyse(_) => "Analyse",
                Self::Configure(_) => "Configure",
                Self::Convert(_) => "Convert",
                Self::Create(_) => "Create",
                Self::Interactive(_) => "Interactive/Run",
                Self::Package(_) => "Package",
                Self::Run(_) => "Interactive/Run",
                Self::Refactor(_) => "Refactor",
                Self::Source(_) => "Process Source",
                Self::Test(_) => "Test",
            }
        )
    }
}

/// Analyse a project
#[derive(Args, Debug)]
pub struct AnalyseCommand {
    #[command(flatten)]
    common_arguments: CommonArguments,

    #[command(flatten)]
    project_arguments: ProjectArguments,
}

/// Update project configuration
#[derive(Args, Debug)]
pub struct ConfigCommand {
    #[command(flatten)]
    common_arguments: CommonArguments,

    #[command(flatten)]
    project_arguments: ProjectArguments,
}

/// Covert a project
#[derive(Args, Debug)]
pub struct ConvertCommand {
    #[command(flatten)]
    common_arguments: CommonArguments,

    #[command(flatten)]
    project_arguments: ProjectArguments,
}

/// Create a project from a source project. Under the provided projects path it creates
/// the full project structure for the project.
#[derive(Args, Debug)]
pub struct CreateCommand {
    /// Project database, stores project details
    #[arg(long = "project-db")]
    project_db: Option<PathBuf>,

    /// Projects path, default path for storing projects
    #[arg(long = "projects-path")]
    projects_path: Option<PathBuf>,

    /// Projects source path, default path for source projects
    #[arg(long = "projects-source-path")]
    projects_source_path: Option<PathBuf>,

    /// Copy source project to <project>/source
    #[arg(long = "copy-source", short = 'C')]
    copy_source: bool,

    /// Project type of the new project, only required if not recognized
    #[arg(long = "project-type", short = 'P')]
    project_kind: Option<ProjectKindArgument>,

    /// Project ID of the new project, a string with no whitespace
    project_id: String,

    /// Project source path of the new project
    source_path: PathBuf,
}

/// Interactive processing or run a Rune script
#[derive(Args, Debug)]
pub struct InteractiveCommand {
    /// Script file to execute
    #[arg(long = "script", short = 's')]
    pub script: Option<PathBuf>,

    /// Keep open when the script ends
    #[arg(long = "keep-open", short = 'K', default_value_t = false)]
    pub keep_open: bool,

    /// Script text to execute
    pub script_text: Vec<String>,
}

impl From<&InteractiveCommand> for ShellSettings {
    fn from(value: &InteractiveCommand) -> Self {
        ShellSettings {
            script: value.script.clone(),
            script_text: value.script_text.clone(),
            keep_open: value.keep_open,
        }
    }
}

/// Package a project for distribution
#[derive(Args, Debug)]
pub struct PackageCommand {
    #[command(flatten)]
    common_arguments: CommonArguments,

    #[command(flatten)]
    project_arguments: ProjectArguments,
}

/// Refactor a project
#[derive(Args, Debug)]
pub struct RefactorCommand {
    #[command(flatten)]
    common_arguments: CommonArguments,

    #[command(flatten)]
    project_arguments: ProjectArguments,
}

/// Run tests on a project
#[derive(Args, Debug)]
pub struct TestCommand {
    #[command(flatten)]
    common_arguments: CommonArguments,

    #[command(flatten)]
    project_arguments: ProjectArguments,
}

/// Project settings
#[derive(Args, Debug)]
pub struct ProjectArguments {
    /// Project path
    project_path: Option<PathBuf>,

    /// Project source path
    #[arg(long = "source-path", short = 's')]
    project_source_path: Option<PathBuf>,
}
